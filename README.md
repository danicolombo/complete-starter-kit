# React + Django + PostgreSQL + Next.js + Material ui + GraphQL + TypeScript + Docker Starter Kit

## Development Setup (Docker)

### Requirements

- Docker
- Docker Compose
- NodeJS and NPM

### Execute

```cp .env .env.dev``` 


Update values as needed



```docker-compose up -d --build```

```docker exec -it web python ./manage.py migrate```

```docker exec -it web python ./manage.py createsuperuser```

```docker-compose exec ui npm install```

```docker-compose exec ui npm run dev```

```docker-compose up```

```
> ui@0.1.0 dev /app
> next dev

ready - started server on http://localhost:3000
```
