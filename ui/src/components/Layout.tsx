import * as React from 'react'
import Link from 'next/link'

const layoutStyle = {
  margin: 20,
  padding: 20,
  border: '1px solid #DDD'
}

const Layout: React.FunctionComponent = ({ children }) => (
  <div style={layoutStyle}>
    <nav>
      <Link href="/">
        <a>Home</a>
      </Link>{' '}
      |{' '}
      <Link href="/about">
        <a>About</a>
      </Link>{' '}
      |{' '}
      <Link href="/admin">
        <a>Django admin</a>
      </Link>{' '}
      |{' '}
      <Link href="/graphql">
        <a>GraphQL interface</a>
      </Link>{' '}
      |{' '}
    </nav>
    {children}
  </div>
)

export default Layout 
