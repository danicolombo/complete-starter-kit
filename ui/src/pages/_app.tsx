import {ApolloProvider} from '@apollo/react-hooks';
import CssBaseline from '@material-ui/core/CssBaseline';
import {ThemeProvider} from '@material-ui/core/styles';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {ApolloClient} from 'apollo-client';
import {HttpLink} from 'apollo-link-http';
import Layout from '../components/Layout';
import withApollo from 'next-with-apollo';
import {AppProps} from 'next/app';
import getConfig from 'next/config';
import Head from 'next/head';
import {useEffect} from 'react';
import * as settings from 'settings';
import {theme} from 'theme';


type CustomAppProps = AppProps & {
    apollo: ApolloClient<any>;
    err?: Error;
};

const App = ({Component, pageProps, apollo, err}: CustomAppProps) => {
    useEffect(() => {
        // Remove the server-side injected CSS.
        const jssStyles = document.querySelector('#jss-server-side');
        if (jssStyles && jssStyles.parentNode) jssStyles.parentNode.removeChild(jssStyles);
    });

    // Workaround for https://github.com/zeit/next.js/issues/8592
    const modifiedPageProps = {...pageProps, err};

    return (
        <>
            <Head>
                <title>{settings.SITE_NAME}</title>
                <meta name='viewport' content='width=device-width, initial-scale=1, user-scalable=no' />
            </Head>
            <ThemeProvider theme={theme}>
                <ApolloProvider client={apollo}>
                    <CssBaseline />
                    <Layout>
                        <Component {...modifiedPageProps} />
                    </Layout>
                </ApolloProvider>
            </ThemeProvider>
        </>
    );
};

export default withApollo(({initialState, ctx, headers}) => {
    const cache = new InMemoryCache().restore(initialState || {});

    const {backendUrl} = getConfig().serverRuntimeConfig;
    const link = new HttpLink({
        uri: `${backendUrl || ''}/graphql/`,
        credentials: 'include',
        headers: {cookie: headers?.cookie},
    });

    return new ApolloClient({
        cache,
        link,
        ssrMode: typeof window === 'undefined',
        defaultOptions: {

        },
    });
})(App);
