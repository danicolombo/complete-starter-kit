import Head from 'next/head';
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import { useRouter } from 'next/router';
import * as settings from '../../settings.ts';

const useStyles = makeStyles(theme => ({
    klein: {
        color: theme.palette.primary.main,
        textDecoration: 'none',
    },
    container: {
        minHeight: '100vh',
        padding:' 0 0.5rem',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        background: '#fff',
    },
    main: {
        padding: '5rem 0',
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    footer: {
        width: '100 %',
        height: '100px',
        borderTop: '1px solid #eaeaea',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        color: theme.palette.primary.main,
        textDecoration: 'none',
    },
    title: {
        color: theme.palette.primary.main,
        textDecoration: 'none',
        margin: '0',
        lineHeight: '1.15',
        fontSize: '4rem',
        textAlign: 'center',
    },
    description: {
        textAlign: 'center',
        lineHeight: 1.5,
        fontSize: '1.5rem',
    },
    code: {
        background: '#fafafa',
        borderRadius: '5px',
        padding: '0.75rem',
        fontSize: '1.1rem',
    },
    grid: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexWrap: 'wrap',
        maxWidth: '800px',
        marginTop: '3rem',
    },
    card: {
        margin: '1rem',
        flexBasis: '45%',
        padding: '1.5rem',
        textAlign: 'left',
        textDecoration: 'none',
        border: '1px solid #eaeaea',
        borderRadius: '10px',
        transition: 'color 0.15s ease, border - color 0.15s ease',
    },
    cardh3: {
        margin: '0 0 1rem 0',
        fontSize: '1.5rem',
    },
    cardp: {
        margin: '0',
        fontSize: '1.25rem',
        lineHeight: '1.5',
    },
}));

export default function Home() {

    const classes = useStyles();
    const router = useRouter();

    return (
        <div className={classes.container}>
            <Head>
                <title>Starter kit</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <main className={classes.main}>
                <Typography variant='h2' className={classes.klein}>
                    Welcome to the <a href="https://gitlab.com/danicolombo/complete-starter-kit" target="_blank">Complete Starter Kit!</a>
                </Typography>

                <p className={classes.description}>
                    Get started by editing{' '}
                    <code className={classes.code}>pages/home/index.tsx</code>
                </p>

                <div className={classes.grid}>
                    <a href="https://www.djangoproject.com/" className={classes.card} target="_blank">
                        <h3 className={classes.cardh3}>Django &rarr;</h3>
                        <p className={classes.cardp}>Use a high-level Python Web framework.</p>
                    </a>

                    <a href="https://graphql.org/" className={classes.card} target="_blank">
                        <h3 className={classes.cardh3}>GraphQL &rarr;</h3>
                        <p className={classes.cardp}>A query language for your Django backend.</p>
                    </a>

                    <a href='https://nextjs.org/' className={classes.card} target="_blank">
                        <h3 className={classes.cardh3}>Next.js &rarr;</h3>
                        <p className={classes.cardp}>And Material UI: full-stack server-side rendered React application.</p>
                    </a>

                    <a
                        href="https://www.typescriptlang.org/"
                        className={classes.card}
                        target="_blank"
                    >
                        <h3 className={classes.cardh3}>TypeScript &rarr;</h3>
                        <p className={classes.cardp}>
                            Add optional types for large-scale JavaScript applications.</p>
                    </a>
                </div>
                <span onClick={() => router.push('/about')}>Click me</span>

            </main>

            <footer className={classes.footer}>
                <a
                    href="https://procnedc.github.io/resume/"
                    target="_blank"
                    rel="Daniela Colombo"
                    className={classes.klein}
                >
                    Made with ♥ by DC
                </a>
            </footer>
        </div>
    )
}
