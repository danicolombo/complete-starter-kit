export const SITE_NAME = 'Starter Kit';

export const LANGUAGES = [{code: 'es-ar', name: 'Español'}];
export const LANGUAGE_CODE = 'es-ar';

export const TIMEZONE = -3;
export const DATE_FORMAT = 'DD/MM/YYYY';
export const DATETIME_FORMAT = 'DD/MM/YYYY HH:mm:ss';

export const REACT_APP_API_ROOT=`http://127.0.1:8080`;

